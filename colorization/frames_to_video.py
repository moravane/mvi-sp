import cv2
import os
import re

frames_folder = 'DATA/colored_frames/chaplin1'
video_folder = 'DATA/colored_videos/' + frames_folder.replace('DATA/colored_frames/', '') + '.avi'

# sort files in dir
numbers = re.compile(r'(\d+)')
def numerical_sort(value):
    parts = numbers.split(value)
    parts[1::2] = map(int, parts[1::2])
    return parts


images = [img for img in os.listdir(frames_folder) if img.endswith(".jpg")]
images.sort(key=numerical_sort)
frame = cv2.imread(os.path.join(frames_folder, images[0]))
height, width, layers = frame.shape
file = open(os.path.join(frames_folder, "fps.txt"), "r")
fps = float(file.read()) # get fps of original greyscale video
fourcc = cv2.VideoWriter_fourcc(*'mp4v') # codec

video = cv2.VideoWriter(video_folder, fourcc, fps, (width, height))

for image in images:
    video.write(cv2.imread(os.path.join(frames_folder, image)))

cv2.destroyAllWindows()
video.release()
