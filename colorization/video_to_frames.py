import cv2
import os

# create a folder to store extracted images
video_name = 'DATA/videosDS/frankenweenie3.mp4'
folder_name = 'DATA/frames/' + video_name.replace('DATA/videosDS/', '').replace('.mp4', '/')
if not os.path.exists(folder_name):
    os.makedirs(folder_name)

# extract frames
video_cap = cv2.VideoCapture(video_name)

fps = video_cap.get(cv2.CAP_PROP_FPS) # get and save fps
file = open(os.path.join(folder_name, "fps.txt"), "w+")
file.write(str(fps))
file.close()

success, image = video_cap.read()
count = 0
while True:
    success, image = video_cap.read()
    if not success:
        break
    cv2.imwrite(os.path.join(folder_name, "{:d}.jpg".format(count)), image) # save frame as JPEG file
    count += 1
print("{} frames are extracted in {}.".format(count, folder_name))
