# DOWNSAMPLING

import moviepy.editor as mp
import os

input_file_path = "DATA/videos/world.mp4"

clip = mp.VideoFileClip(input_file_path)
# make the height 360px ( According to moviePy documentation the width is then computed so that
# the width/height ratio is conserved.)
clip_resized = clip.resize(height=360)
clip_resized.write_videofile(os.path.join('DATA/videosDS/', input_file_path.replace('DATA/videos/', '')))
