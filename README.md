# Dobarvování černobílých filmů pomocí konvolučních neuronových sítí

### Zadání

Stáhněte si neuronovou sít natrénovanou na dobarvování černobílých obrázků 
(http://richzhang.github.io/colorization/) a aplikujte ji na černobílé filmy. 
Zkontrolujte, že síť správně obarvuje korespondující oblasti v dvou následujících
framech konzistentně (př.: obloha mezi snímky nezmění barvu z modré na zelenou) 
a případně implementujte algoritmus na stabilizaci barev.

* Článek: https://arxiv.org/pdf/1603.08511.pdf
* Podobný projekt: https://vimeo.com/151159255

### Report obsahující videa
https://drive.google.com/open?id=1S6SdVY8NrCR4tCEWyz3FKR8E1dc7y0La

### Pokyny
* Spustit colorization/models/fetch_release_models.sh pro stažení modelu
* Ve složce colorization/DATA/videosDS je příklad černobílého videa a jeho obarvená verze je v colorization/DATA/colored_videos
* Spustit colorization/main.py - v kódu ve funkci main() nutno nastavit proměnnou input_file_path na cestu k vstupnímu videu (vstupní videa musí být v colorization/DATA/videos) 